(defproject org.clojars.august/sparrows "0.3.3"
  :description "A utility library providing encryption/decryption, io utils and more."
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :profiles {:provided
             {:dependencies
              [[org.clojure/clojure "1.10.1"]
               [http-kit "2.5.3"]
               [clj-http "3.12.0"]
               [com.climate/claypoole "1.1.4"]
               [com.taoensso/timbre "5.1.2"]]}}
  :dependencies [[commons-codec "1.15"]
                 [commons-io/commons-io "2.8.0"]
                 [org.apache.commons/commons-email "1.5"]]
  :omit-source false
  :deploy-repositories [["releases" :clojars]
                        ["snapshots" :clojars]]
  :plugins [[lein-ancient "0.7.0"]]
  :java-source-paths ["src/java"])
